 package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Seccion1Application {

	public static void main(String[] args) {
		SpringApplication.run(Seccion1Application.class, args);
	}

}
