package com.example.demo.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.models.Usuario;



@Controller
@RequestMapping("/app")
public class IndexController {
@Value("${texto.indexcontroller.index.titulo}")
	private String textoIndex;

@Value("${texto.indexcontroller.perfil.titulo}")	
	private String textoPerfil;

@Value("${texto.indexcontroller.listar.titulo}")
	private String textoListar;
	
	
	
	
	
	
	
	@GetMapping({"/","/index","","/home"})
	public String index(Model model) {
model.addAttribute("titulo",textoIndex);
		
		
		return "index";
}
	@RequestMapping("/perfil")
	public String perfil(Model model) {
		model.addAttribute("titulo",textoPerfil);
		return "perfil";
		
		
	}
	@RequestMapping("/listar")
	public String listar(Model model) {





model.addAttribute("titulo",textoListar);
	
		return "listar";
		
		

	}
	@ModelAttribute("usuarios")
	public List<Usuario>poblarUsuarios(){
		
		List<Usuario>usuarios=Arrays.asList(new Usuario("Carlos","Evens","carlos.evens@cicpm.cl"),
				new Usuario("Alonso","Stange","alonso.stange@cicpm.cl"),
				new Usuario("Felipe","Vergara","felipe.vergara@cicpm.cl"));
	
return usuarios;
	}
	
}
